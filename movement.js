
let character = document.querySelector('.character');
character.style.position = "relative";
character.style.left = "200px";
character.style.top = "20px";

function input(event){

    if(event.type === "keydown"){
        switch(event.keyCode){
            case 37:
                console.log("left arrow pressed");
                moveLeft();
                break;
            case 38:
                console.log("up arrow pressed");
                moveUp();
                break;
            case 39:
                console.log("right arrow presse");
                moveRigth();
                break;
            case 40:
                console.log("down arrow pressed");
                moveDown();
                break;
            default:
                console.log("no movement key pressed");
        }
    }
}


window.addEventListener('keyup',input);
window.addEventListener('keydown',input);

function moveDown(){
    character.style.top = parseInt(character.style.left) - 5 +"px";
}

function moveUp(){
    character.style.top = parseInt(character.style.top) - 5 +"px";
}

function moveRigth(){
    character.style.left = parseInt(character.style.left) + 3 +"px";
}

function moveLeft(){
    character.style.left = parseInt(character.style.left) - 5 +"px";
}