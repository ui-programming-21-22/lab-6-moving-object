console.log("hello console ")

function pageLoader()
{
    var canvas = document.getElementById("thecanvas");
    var context = canvas.getContext("2d");
    context.fillRect(1000,400,50,50);
}

pageLoader();


function changeColour() {
    const rectBox = document.querySelector('#rect');
    let randomColor = Math.floor(Math.random()*16777215).toString(16); 

    randomColor = "#"+randomColor;
    rectBox.style.backgroundColor = randomColor;
    let extraText = document.querySelector(".colourCode");
    extraText.innerHTML = randomColor;
}


document.addEventListener("DOMContentLoaded", function(event) {
    let clickBox = document.querySelector('.clickBox');
    clickBox.addEventListener("click", function(e) {
        console.log(e);
        let innerClickBox = document.querySelector('.innerClickBox');
        innerClickBox.innerHTML = "click x: " + e.x + '<br>' + "click y: " + e.y + '<br>' + "altKey held: " + e.altKey + '<br>' + "ctrlKey held: " + e.ctrlKey;
    });
});
